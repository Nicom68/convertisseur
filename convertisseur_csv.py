import pandas as pd
from sqlalchemy import create_engine


# 1/ conversion csv en excel


# def csv_to_excel(csv_file, excel_file):
#     # Read the CSV file
#     df = pd.read_csv(csv_file)
    
#     # Convert to Excel
#     df.to_excel(excel_file, index=False)
   

# Test
# csv_to_excel('test.csv', 'test.xlsx')

# 2/ conversion csv en xml

# def csv_to_xml(csv_file,xml_file):
#     # lecture du fichier csv
#     df = pd.read_csv(csv_file, index=False)

#     # conversion du fichier excel en fichier xml
#     df.to_xml(xml_file,index=False)

# # Test
# csv_to_xml('books.csv', 'books.xml')


# 3/ conversion csv en json


# def csv_to_json( csv_file,json_file):
#     # lecture du fichier csv
#     df = pd.read_csv(csv_file)
    
# Convert to json
#     df.to_json(json_file, index=False)

# Test
# csv_to_json('people.xlsx','people.json')


# 4/ conversion csv en sql

# def csv_to_sql(csv_file, sql_table_name, db_url):
#     # lecture du fichier csv
#     df = pd.read_csv(csv_file)
    
#     #Création du moteur SQL ALCHEMY
#     engine = create_engine(db_url)
    
#     #écrire le dataframe dans une table SQL
#     df.to_sql(sql_table_name, engine, if_exists='replace', index=False)

#  # Test
# csv_file = 'books.csv'
# sql_table_name = 'books'
# db_url = 'sqlite:///books.db'  # Example SQLite database URL
# csv_to_sql(csv_file, sql_table_name, db_url)
