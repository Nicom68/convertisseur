import pandas as pd
from sqlalchemy import create_engine


# 1/ conversion sql en excel

# def sql_to_excel(sql_table_name, db_url, excel_file):
#     # création du moteur sqllite
#     engine = create_engine(db_url)
    
#     # lecture de la table SQL dans un dataframe
#     df = pd.read_sql_table(sql_table_name, engine)
    
#     # Ecriture du dataframe dans un fichier excel
#     df.to_excel(excel_file, index=False)

# Test
# sql_table_name = 'people'
# db_url = 'sqlite:///people.db'  # Exemple de base de données Sqlite
# excel_file = 'people.xlsx'
# sql_to_excel(sql_table_name, db_url, excel_file)

# 2/ conversion sql en json

# def sql_to_json(sql_table_name, db_url, json_file):
#     # création du moteur sqlite
#     engine = create_engine(db_url)
    
#     # lecture de la table SQL dans un dataframe
#     df = pd.read_sql_table(sql_table_name, engine)
    
#     # Ecriture du dataframe dans un fichier json
#     df.to_json(json_file, index=False)

# # Test
# sql_table_name = 'books'
# db_url = 'sqlite:///books.db'  # Exemple de base de données Sqlite
# json_file = 'books.json'
# sql_to_json(sql_table_name, db_url, json_file)

# 3/ conversion sql en csv

# def sql_to_csv(sql_table_name, db_url, csv_file):
#     # création du moteur sqllite
#     engine = create_engine(db_url)
    
#     # lecture de la table SQL dans un dataframe
#     df = pd.read_sql_table(sql_table_name, engine)
    
#     # Ecriture du dataframe dans un fichier csv
#     df.to_csv(csv_file, index=False)

# # Test
# sql_table_name = 'books'
# db_url = 'sqlite:///books.db'  # Exemple de base de données Sqlite
# csv_file = 'books.csv'
# sql_to_csv(sql_table_name, db_url, csv_file)


# 4/ conversion sql en xml

def sql_to_xml(sql_table_name, db_url, xml_file):
    # création du moteur sqllite
    engine = create_engine(db_url)
    
    # lecture de la table SQL dans un dataframe
    df = pd.read_sql_table(sql_table_name, engine)
    
    # Ecriture du dataframe dans un fichier xml
    df.to_xml(xml_file, index=False)

# Test
sql_table_name = 'books'
db_url = 'sqlite:///books.db'  # Exemple de base de données Sqlite
xml_file = 'books.xml'
sql_to_xml(sql_table_name, db_url, xml_file)





