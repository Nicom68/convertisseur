import pandas as pd
from sqlalchemy import create_engine


def sql_to_file(sql_table_name, db_url, file_path, file_format):
    # Création du moteur SQLAlchemy
    engine = create_engine(db_url)
    
    # lecture de la table SQL dans un dataframe
    df = pd.read_sql_table(sql_table_name, engine)
    
    # Conversion du dataframe dans les formats demandés (xlsx, json, xml, sql)
    if file_format == 'xlsx':
        df.to_excel(file_path, index=False, engine='openpyxl')
    elif file_format == 'json':
        df.to_json(file_path, orient='records')
    elif file_format == 'csv':
        df.to_csv(file_path, index=False)
    elif file_format == 'xml':
        df.to_xml(file_path, index=False)
    else:
        print("Ce format n'est pas lisible.")

# Test
sql_table_name = 'books'  
db_url = 'sqlite:///books.db'
file_path = 'books.xml'
file_format = 'xml'
sql_to_file(sql_table_name, db_url, file_path, file_format)