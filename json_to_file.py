import pandas as pd
from sqlalchemy import create_engine


def json_to_file(json_file, file_path, file_format, sql_table_name=None, db_url=None):
    # lecture du fichier json dans un dataframe
    df = pd.read_json(json_file)
    
    # Conversion du dataframe dans les format dans les formats demandés (csv,xlsx, xml, sql)
    if file_format == 'csv':
        df.to_csv(file_path, index=False)
    elif file_format == 'xlsx':
         df.to_excel(file_path, engine='openpyxl')
    elif file_format == 'xml':
        df.to_xml(file_path, index=False)
    elif file_format == 'sql' and sql_table_name and db_url:
        # Create SQLAlchemy engine
        engine = create_engine(db_url)
        
        # Write the DataFrame to a SQL table
        df.to_sql(sql_table_name, engine, if_exists='replace', index=False)
        
        # Génère un SQL dump à partir du dataFrame et crée un fichier
        output_dump_path = file_path
        with engine.connect() as conn:
            sql_dump = '\n'.join(conn.connection.iterdump())
            with open(output_dump_path, 'w') as sql_file:
                sql_file.write(sql_dump)
    else:
        print("Ce fichier n'est pas lisible.")

# Test
json_file = 'people.json'
file_path = 'people.csv'
file_format = 'csv'
sql_table_name = 'people'
db_url = 'sqlite:///people.db'
json_to_file(json_file, file_path, file_format, sql_table_name, db_url)