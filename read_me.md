Exercice convertisseur

Première partie :
Nous avons d'abord transformé un fichier au format csv en des fichiers au format xlsx, json, xml, db. Nous avons procédé de même pour successivement un fichier excel (xlsx), json, xml, db.

Vous trouverez le code relatif à cette partie, dans les fichiers convertisseur_csv.py, convertisseur_excel.py, convertisseur_json.py, convertisseur_sql.py,convertisseur_xml.py.

Deuxième partie :
Nous avons  tenté de définir une fonction capable, à partir d'un fichier source au format csv,  de générer des fichiers au format (xlsx,json, sql, xml)
Nous avons procédé de même pour des fichiers source au format xlsx, json, sql, et xml.
Vous trouverez le code relatif à cette partie dans les fichiers :
- csv_to_file.py
- excel_to_file.py
- json_to_file.py
- sql_to_file.py
- xml_to_file.py

Troisième partie :
Nous avions réussi à créer une fonction qui génère à partir d'un fichier source de format spécifique d'autres formats. Dans la 3ème partie, nous nous avons tenté de constuire une fonction sur le modèle précédent capable à partir de fichier source de format non prédéfini à l'avance de générer des fichiers au format (xlsx, csv, xml, json, sql)

Le code relatif à cette fonction se trouve dans le fichier file_to_files.py
