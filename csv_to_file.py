import pandas as pd
from sqlalchemy import create_engine

def csv_to_file(csv_file, file_path, file_format, sql_table_name=None, db_url=None):
    # Lit le fichier dans un dataframe
    df = pd.read_csv(csv_file)
    
    # Conversion du dataframe dans les formats demandés (xlsx, json, xml, sql)
    if file_format == 'xlsx':
        df.to_excel(file_path, index=False)
    elif file_format == 'json':
        df.to_json(file_path, orient='records')
    elif file_format == 'xml':
        df.to_xml(file_path, index=False)
    elif file_format == 'sql' and sql_table_name and db_url:
        # Create SQLAlchemy engine
        engine = create_engine(db_url)
        
        # Write the DataFrame to a SQL table
        df.to_sql(sql_table_name, engine, if_exists='replace', index=False)
        
        # Génère un SQL dump à partir du dataFrame et crée un fichier
        output_dump_path = file_path
        with engine.connect() as conn:
            sql_dump = '\n'.join(conn.connection.iterdump())
            with open(output_dump_path, 'w') as sql_file:
                sql_file.write(sql_dump)
    else:
        print("Ce fichier n'est pas lisible.")

# Test
csv_file = 'books.csv'
file_path = 'books.sql'
file_format = 'sql'
sql_table_name = 'books'
db_url = 'sqlite:///books.db'
csv_to_file(csv_file, file_path, file_format, sql_table_name, db_url)