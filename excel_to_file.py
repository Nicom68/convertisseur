import pandas as pd
from sqlalchemy import create_engine

def excel_to_file(excel_file, file_path, file_format, sql_table_name=None, db_url=None):
    # Read the Excel file into a DataFrame
    df = pd.read_excel(excel_file, engine='openpyxl')
    
    # Conversion of the DataFrame into different formats (csv, json, xml, sql)
    if file_format == 'csv':
        df.to_csv(file_path, index=False)
    elif file_format == 'json':
        df.to_json(file_path, orient='records')
    elif file_format == 'xml':
        df.to_xml(file_path, index=False)
    elif file_format == 'sql' and sql_table_name and db_url:
        # Create SQLAlchemy engine
        engine = create_engine(db_url)
        
        # Write the DataFrame to a SQL table
        df.to_sql(sql_table_name, engine, if_exists='replace', index=False)
        
        # Génère un SQL dump à partir du dataFrame et crée un fichier
        output_dump_path = file_path
        with engine.connect() as conn:
            sql_dump = '\n'.join(conn.connection.iterdump())
            with open(output_dump_path, 'w') as sql_file:
                sql_file.write(sql_dump)
    else:
        print("Ce fichier n'est pas lisible.")

# Test
excel_file = 'books.xls'
file_path = 'books.xml'
file_format = 'xml'
sql_table_name = 'books'
db_url = 'sqlite:///books.db'
excel_to_file(excel_file, file_path, file_format, sql_table_name, db_url)