import pandas as pd
from sqlalchemy import create_engine

def file_to_file(input_file, output_file, output_format, sql_table_name=None, db_url=None):
    # Detect input file format based on file extension
    input_extension = input_file.split('.')[-1]
    
    # lecture du fichier dans un dataframe
    if input_extension == 'xml':
        df = pd.read_xml(input_file)
    elif input_extension == 'xlsx':
        df = pd.read_excel(input_file, engine='openpyxl')
    elif input_extension == 'json':
        df = pd.read_json(input_file)
    elif input_extension == 'csv':
        df = pd.read_csv(input_file)
    elif input_extension == 'sql':
        # lecture de la table SQL dans un dataframe
        engine = create_engine(db_url)
        df = pd.read_sql_table(sql_table_name, engine)
    else:
        print("Format de fichier non pris en charge.")
        return
    
    # Conversion du dataframe dans les formats demandés (csv, xlsx, xml, json,sql)
    if output_format == 'csv':
        df.to_csv(output_file, index=False)
    elif output_format == 'xlsx':
         df.to_excel(output_file, engine='openpyxl', index=False)
    elif output_format == 'xml':
        df.to_xml(output_file, index=False)
    elif output_format =='json':
        df.to_json(output_file, index=False)
    elif output_format == 'sql' and sql_table_name and db_url:
        engine = create_engine(db_url)
        df.to_sql(sql_table_name, engine, if_exists='replace', index=False)
        # Génère un dump SQL à partir du dataframe
        output_dump_path = output_file
        with engine.connect() as conn:
            sql_dump = '\n'.join(conn.connection.iterdump())
            with open(output_dump_path, 'w') as sql_file:
                sql_file.write(sql_dump)
    else:
        print("Format de fichier de sortie non pris en charge.")

# Test
input_file = 'people.json'
output_file = 'people.sql'
output_format = 'sql'
sql_table_name = 'people'
db_url = 'sqlite:///people.db'
file_to_file(input_file, output_file, output_format, sql_table_name, db_url)