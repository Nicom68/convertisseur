import pandas as pd
from sqlalchemy import create_engine
import lxml


#1/ conversion xml en excel

# def xml_to_excel(xml_file, excel_file):
#     # lecture du fichier xml
#     df=pd.read_xml(xml_file)

#     # conversion du fichier xml_file en fichier excel
#     df.to_excel(excel_file, engine='openpyxl',index=False)
  

# # Test
# xml_to_excel('books.xml','books.xls')


# 2/ conversion xml en csv

# def xml_to_csv(csv_file, xml_file):
#     # lecture du fichier xml
#     df=pd.read_xml(xml_file)

#     # conversion du fichier xml_file en fichier csv
#     df.to_csv(csv_file,index=False)
  

# # Test
# xml_to_csv('books.xml','books.csv')


# 3/ conversion xml en sql

def xml_to_sql(xml_file, sql_table_name, db_url):
    # lecture du fichier xml
    df = pd.read_xml(xml_file)
    
    # Création du moteur SQL ALCHEMY
    engine = create_engine(db_url)
    
    # écrire le dataframe dans une table SQL
    df.to_sql(sql_table_name, engine, if_exists='replace', index=False)

# Test
xml_file = 'books.xml'
sql_table_name = 'books'
db_url = 'sqlite:///books.db'  # Example SQLite database URL
xml_to_sql(xml_file, sql_table_name, db_url)


# 4/ conversion xml en json


# def xml_to_json( xml_file,json_file):
#     # lecture du fichier xml
#     df = pd.read_xml(xml_file)
    
#     # Convert to json
#     df.to_json(json_file, index=False)

# # Test
# xml_to_json('books.xml','books.json')
