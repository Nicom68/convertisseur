import pandas as pd
from sqlalchemy import create_engine

# 1/ conversion json en excel
# def json_to_excel(json_file, excel_file):
#     # Read the JSON file
#     df = pd.read_json(json_file)
    
    # Convert to Excel
#     df.to_excel(excel_file, index=False)

# 2/ conversion json en xml

# def json_to_xml(json_file, xml_file):
#     # lecture du fichier json
#     df = pd.read_json(json_file)

#     # Conversion du dataframe en XML a
#     df.to_xml(xml_file, index=False)

# # Test the function
# json_to_xml('books.json', 'books.xml')

# 3/ conversion json en csv

# def json_to_csv(json_file, csv_file):
#     # lecture du fichier json
#     df=pd.read_json(json_file)

#     # conversion du fichier json_file en fichier csv
#     df.to_csv(csv_file,index=False)
  

# # Test
# json_to_csv('books.json','books.csv')

# 4/ conversion json en sql

def json_to_sql(json_file, sql_table_name, db_url):
        # lecture du fichier json
    df = pd.read_json(json_file)
    
       # Création du moteur SQL ALCHEMY
    engine = create_engine(db_url)
    
        # écrire le dataframe dans une table SQL
    df.to_sql(sql_table_name, engine, if_exists='replace', index=False)

# Test
json_file = 'books.json'
sql_table_name = 'books'
db_url = 'sqlite:///books.db'  # Example SQLite database URL
json_to_sql(json_file, sql_table_name, db_url)


