
import pandas as pd
from sqlalchemy import create_engine


# 1/ conversion excel en csv

# def excel_to_csv(excel_file, csv_file):
#     # lecture du fichier excel
#     df = pd.read_excel(excel_file, engine='openpyxl')
    
#     # convertir en csv
#     df.to_csv(csv_file, index=False)
#    

# test
# excel_to_csv('test.xlsx', 'test.csv')





# 2/ conversion excel en json

# def excel_to_json( excel_file,json_file):
#     # lecture du fichier excel
#     df = pd.read_excel(excel_file)
    
# Convert to json
#     df.to_json(json_file, index=False)

# Test
# excel_to_json('people.xlsx','people.json')


# 3/ conversion excel en sql

# def excel_to_sql(excel_file, sql_table_name, db_url):
    # lecture du fichier excel
#     df = pd.read_excel(excel_file)
    
       # Création du moteur SQL ALCHEMY
#     engine = create_engine(db_url)
    
        # écrire le dataframe dans une table SQL
#     df.to_sql(sql_table_name, engine, if_exists='replace', index=False)

# Test
# excel_file = 'people.xlsx'
# sql_table_name = 'people'
# db_url = 'sqlite:///people.db'  # Example SQLite database URL
# excel_to_sql(excel_file, sql_table_name, db_url)





# 4/ conversion excel en xml

def excel_to_xml(excel_file,xml_file):
    # lecture du fichier excel
    df = pd.read_excel(excel_file, engine='openpyxl')

    # conversion du fichier excel en fichier xml
    df.to_xml(xml_file,index=False)

# Test
excel_to_xml('books.xls', 'books.xml')
